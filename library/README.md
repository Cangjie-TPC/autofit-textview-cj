1. 用途：自动调整文本大小以完全适合其边界的TextView。
2. 参考源码：https://gitee.com/openharmony-sig/ohos-autofittextview/blob/master/library/src/main/ets/AutofitTextView.ets
   1. 改动点：Autofittextview主要参考开源软件改写或来自开源，本次外发主要涉及UI界面库函数代码实现，不涉及华为业务逻辑，属于非关键代码-Demo代码。

│  build-profile.json5                         #配置文件，构建用
│  BuildProfile.ets                              #配置文件，构建用
│  hvigorfile.ts                                    #配置文件，构建用
│  oh-package.json5                         #配置文件，构建用
│  README.md                                  #介绍
│
└─src
    └─main
        │  module.json5                       #配置文件，构建用
        │
        └─cangjie
            │  cjpm.lock                          #配置文件，构建用
            │  cjpm.toml                         #配置文件，构建用
            │
            └─src
                    autofit_text_view.cj      #库文件，主页面
                    package.cj                      #包调用
                    typed_value.cj               #常数变量

