
## API接口文档

### class AutofitTextView

AutofitTextView是一个自动调整文本大小以适应给定宽度的自定义控件。

- **构造函数**:
  ```cangjie
  AutofitTextView(model: Model)
  ```
    - `model`: Model对象，包含AutofitTextView的配置信息。

- **方法**:
    - `build()`: 构建AutofitTextView视图。

### class Model

Model类用于存储和管理AutofitTextView的状态和属性。

- **属性**:
    - `textSize`: Float64，当前字体大小，默认值为20.0。
    - `minTextSize`: Float64，最小字体大小，默认值为10.0。
    - `maxTextSize`: Float64，最大字体大小，默认值为40.0。
    - `maxLines`: Int32，最大行数，默认值为1。
    - `content`: String，文本内容，默认值为"123"。
    - `width`: Length，控件宽度，默认值为100%。
    - `isEnabled`: Bool，是否启用自动调整大小，默认值为true。
    - `precision`: Int64，精度，默认值为10。
    - `backgroundColor`: UInt32，背景颜色，默认值为0XFFFFFF（白色）。
    - `overFlow`: TextOverflow，文本溢出处理方式，默认值为TextOverflow.Clip。

- **方法**:
    - `setTextSize(size: Float64, unit: Int64 = TypedValue.UNDEFINED)`: 设置字体大小。
    - `setMaxLines(lines: Int32)`: 设置最大行数。
    - `setBackgroundColor(color: UInt32)`: 设置背景颜色。
    - `setTextOverflow(flow: TextOverflow)`: 设置文本溢出处理方式。
    - `setText(text: String)`: 设置文本内容。
    - `setWidth(width: Length)`: 设置控件宽度。
    - `setSizeToFit(sizeToFit: Bool)`: 设置是否启用自动调整大小。
    - `getMaxTextSize()`: 获取最大字体大小。
    - `setMinTextSize(minSize: Float64, unit: Int64 = TypedValue.UNDEFINED)`: 设置最小字体大小。
    - `getMinTextSize()`: 获取最小字体大小。
    - `getPrecision()`: 获取精度。
    - `getTextSize()`: 获取当前字体大小。
    - `setPrecision(precision: Int64)`: 设置精度。
