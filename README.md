<div align="center">
<h1>autofit-textview-cj</h1>
</div>


<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.56.4-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## 介绍

一个能自动调整文字大小以完美贴合其显示边界的 TextView [android-autofittextview](https://gitcode.com/gh_mirrors/an/android-autofittextview) 的 Cangjie 移植版本。

### 特性

- 🚀 自动调整文字大小以完美贴合其显示边界


## 软件架构

### 源码目录

```shell
├─AppScope
├─doc
├─entry
│  └─src
│      └─main
│          ├─cangjie
│          └─resources
├─autofittextview4cj
│  └─src
│      └─main
│          ├─cangjie
│          │  └─src
│          └─resources
└─hvigor

```

- `AppScope` 全局资源存放目录和应用全局信息配置目录
- `doc` API文档和使用手册存放目录
- `entry` 工程模块 - 编译生成一个HAP
- `entry src` APP代码目录
- `entry src main` APP项目目录
- `entry src main cangjie` 仓颉代码目录
- `entry src main resources` 资源文件目录
- `autofittextview4cj` 工程模块 - 编译生成一个har包
- `autofittextview4cj src` 模块代码目录
- `autofittextview4cj src main` 模块项目目录
- `autofittextview4cj src main cangjie` 仓颉代码目录
- `autofittextview4cj src main resources` 资源文件目录
- `hvigor` 构建工具目录

### 接口说明

主要类和函数接口说明详见 [API](doc/feature_api.md)

## 使用说明

### 编译构建

1. 通过module引入
    1. 克隆下载项目
    2. 将library模块拷贝到应用项目下
    3. 修改自身应用 entry 下的 oh-package.json5 文件，在 dependencies 字段添加 "autofittextview4cj": "file:../autofittextview4cj"
       ```json
       {
          "name": "entry",
          "version": "1.0.0",
          "description": "Please describe the basic information.",
          "main": "",
          "author": "",
          "license": "",
         "dependencies": {
             "autofittextview4cj": "file:../autofittextview4cj"
         }
       }
       ```
    4. 修改自身应用 entry/src/main/cangjie 下的 cjpm.toml 文件，在 [dependencies] 字段下添加
        ``` toml
        [dependencies.autofittextview4cj]
          path = "../../../../autofittextview4cj/src/main/cangjie"
          version = "1.0.0"
        ```
       toml变为
        ``` toml
        [dependencies]
          [dependencies.cj_res_entry]
            path = "./cj_res"
            version = "1.0.0"
          [dependencies.autofittextview4cj]
            path = "../../../../autofittextview4cj/src/main/cangjie"
            version = "1.0.0"
        ```
    5. 在项目中使用 import autofittextview4cj.* 引用该项目
       ```cangjie
       import autofittextview4cj.*
       ```
### 预览效果

<img src="./doc/image/autofittextview.gif" width="30%"/>

### 功能示例
```cangjie
@Entry
@Component
class EntryView {
    @State
    var message: String= "Hello Cangjie"
    @State
    var model: Model= Model()
    @State
    var model1: Model= Model()
    @State
    var mWidth: Length= 100.percent
    @State
    var historyInput: String= "This is an example"
    @State
    var normalContent: String= ''
    @State
    var content: String= ''

    protected override func aboutToAppear() {
        this.model.setWidth(this.mWidth)
        this.model.setText(this.historyInput)
        this.model.setMaxLines(2)
        this.model.setTextSize(22.0, unit: TypedValue.COMPLEX_UNIT_PX)
        this.model.setMinTextSize(16.0)
        this.model.setMaxTextSize(50.0)
        this.model.setTextOverflow(TextOverflow.None)

        this.model1.setWidth(this.mWidth)
        this.model1.setText(this.historyInput)
        this.model1.setMaxLines(2)
        this.model1.setTextSize(22.0, unit: TypedValue.COMPLEX_UNIT_FP)
        this.model1.setMinTextSize(16.0)
        this.model1.setMaxTextSize(40.0)
        this.model1.setBackgroundColor(0XD1C9C9)
        this.model1.setTextOverflow(TextOverflow.Ellipsis)

        message = "MaxTextSize: "
        return
    }

    func build() {
        Row {
            Column {
              TextInput(placeholder: '请输入...', text: this.historyInput)
                .placeholderColor(Color.GRAY)
                .fontColor(Color.BLACK)
                .enterKeyType(EnterKeyType.EntrySearch)
                .caretColor(Color.GREEN)
                .height(60)
                .padding(10)
                .fontSize(20)
                .borderRadius(12)
                .borderColor(0xc8bebe)
                .borderWidth(2)
                .onChange({content: String =>
                  this.historyInput = content
                  this.model.setText(content)
                  this.model1.setText(content)
                  this.normalContent = content
                });

              Text('Autofit:').fontSize(20).margin(top: 20);

              AutofitTextView(model: this.model);

              AutofitTextView(model: this.model1);

              Text('Normal:').fontSize(20).margin(top: 20);

              Text(this.historyInput).fontSize(16).margin(top: 10)
                .maxLines(2).textOverflow(TextOverflow.Ellipsis);

              Text("${this.model.getTextSize()}").fontSize(20).margin(top: 20);
              Text("${this.model1.getTextSize()}").fontSize(20).margin(top: 20);

                }.width(100.percent)
        }.height(100.percent)

    }
}
```

## 约束与限制

1. 当前基于 deveco-studio-5.0.3.910 版本实现

## 开源协议

本项目基于 [Apache License 2.0](./LICENSE) ，请自由的享受和参与开源。

## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。

